# uTarget

uTarget is a simple utility that can be used to bulk add `target` attributes to all the hyperlinks within a target
HTML node or the entire document.

## Basic usage

Simply run the utility:

    Mingos.uTarget();

The default settings will target all links in the document and add `target="_blank"` to all external links, while all
internal links will have `target="_self"`. Hyperlinks with a target previously set will remain unaltered.

If you wish to further configure how the script behaves, you can pass a config object to the `run()` function.

## Config options

### `target`

If you do not want to modify all links in the document, simply specify the node or nodes you find appropriate. The
target can be a single `Node`, an array thereof, a `NodeList` (returned by `document.querySelectorAll()`) or a string,
which will then become an argument for `document.querySelectorAll()`.

    Mingos.uTarget({
        target: document.getElementById("content")
    });

    Mingos.uTarget({
        target: [
            document.getElementById("content"),
            document.getElementById("sidebar")
        ]
    });

    Mingos.uTarget({
        target: document.querySelectorAll("article")
    });

    Mingos.uTarget({
        target: "article"
    });

### `rules`

You may want to specify your own rules for certain links. For example, you may want to open a host "example.com" in the
same window, even though it's an external link. All you need to do is to pass an object containing hosts as keys and
the desired `target` values:

    Mingos.uTarget({
        rules: {
            "example.com": "_self"
        }
    });

Rules can also be regexes. In such a case, they will be treated as the first parametre to the `RegExp` constructor, ie.
they mustn't contain regex delimiters or flags. For example, if you would like to match all subdomains of a given
domain, you could do something like this:

    Mingos.uTarget({
        rules: {
            "\\.example\\.com$": "_self"
        }
    });

## Return value

The `uTarget()` function will always return a numeric value indicating the number of hyperlinks that were altered.
