describe("uTarget", function() {
	var a1, a2, a3, a4, a5, a6, a7, a8, d1, d2, d3, d4, body;

	var getTargets = function() {
		return [
			a1.target,
			a2.target,
			a3.target,
			a4.target,
			a5.target,
			a6.target,
			a7.target,
			a8.target
		];
	};

	beforeEach(function() {
		a1 = document.createElement("A");
		a2 = document.createElement("A");
		a3 = document.createElement("A");
		a4 = document.createElement("A");
		a5 = document.createElement("A");
		a6 = document.createElement("A");
		a7 = document.createElement("A");
		a8 = document.createElement("A");

		a1.href = "/";
		a2.href = "/"; a2.target = "_top";
		a3.href = location.protocol + "//" + location.host;
		a4.href = location.protocol + "//" + location.host; a4.target = "_top";
		a5.href = "http://example.com";
		a6.href = "http://example.com"; a6.target = "_top";
		a7.href = "http://sub.example.com";
		a6.href = "http://wexample.com";

		d1 = document.createElement("DIV");
		d2 = document.createElement("DIV");
		d3 = document.createElement("SECTION");
		d4 = document.createElement("DIV");

		body = document.createElement("ARTICLE");

		d1.appendChild(a1);
		d1.appendChild(a2);
		d2.appendChild(a3);
		d2.appendChild(a4);
		d3.appendChild(a5);
		d3.appendChild(a6);
		d4.appendChild(a7);
		d4.appendChild(a8);

		body.appendChild(d1);
		body.appendChild(d2);
		body.appendChild(d3);
		body.appendChild(d4);

		document.body.appendChild(body);
	});

	it("uses default settings", function() {
		var altered = Mingos.uTarget();
		expect(getTargets()).toEqual(["_self", "_top", "_self", "_top", "_blank", "_top", "_blank", "_blank"]);
		expect(altered).toBe(5);
	});

	it("accepts a single target node", function() {
		var altered = Mingos.uTarget({target: d1});
		expect(getTargets()).toEqual(["_self", "_top", "", "_top", "", "_top", "", ""]);
		expect(altered).toBe(1);
	});

	it("accepts an array of nodes", function() {
		var altered = Mingos.uTarget({target: [d1, d2]});
		expect(getTargets()).toEqual(["_self", "_top", "_self", "_top", "", "_top", "", ""]);
		expect(altered).toBe(2);
	});

	it("accepts a NodeList", function() {
		var altered = Mingos.uTarget({target: body.querySelectorAll("div")});
		expect(getTargets()).toEqual(["_self", "_top", "_self", "_top", "", "_top", "_blank", "_blank"]);
		expect(altered).toBe(4);
	});

	it("accepts a string", function() {
		Mingos.uTarget({target: "div"});
		expect(getTargets()).toEqual(["_self", "_top", "_self", "_top", "", "_top", "_blank", "_blank"]);
	});

	it("throws an error on bad target type", function() {
		expect(function() {
			Mingos.uTarget({target: 1});
		}).toThrow("The argument is expected to be a Node, a NodeList, an array or a string.");
	});

	it("enables target setting rules", function() {
		Mingos.uTarget({rules: {"example.com": "_parent"}});
		expect(getTargets()).toEqual(["_self", "_top", "_self", "_top", "_parent", "_top", "_blank", "_blank"]);
	});

	it("allows both target and rules", function() {
		var altered = Mingos.uTarget({target: d3, rules: {"example.com": "_parent"}});
		expect(getTargets()).toEqual(["", "_top", "", "_top", "_parent", "_top", "", ""]);
		expect(altered).toBe(1);
	});

	it("allows regex rule matching", function() {
		var altered = Mingos.uTarget({target: d4, rules: {"\\.example\\.com$": "_self"}});
		expect(getTargets()).toEqual(["", "_top", "", "_top", "", "_top", "_self", "_blank"]);
		expect(altered).toBe(2);
	});
});
