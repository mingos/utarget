module.exports = function(grunt) {
	var pkg = grunt.file.readJSON("package.json"),
		banner = "/*! uTarget <%= pkg.version %>\n" +
			" *  Copyright (c) 2014 Dominik Marczuk\n" +
			" *  Licence: BSD-3-Clause: http://opensource.org/licenses/BSD-3-Clause\n" +
			" */\n";

	grunt.initConfig({
		pkg: pkg,
		concat: {
			dist: {
				options: {
					banner: banner +
						"(function(window) {\n" +
						"\"use strict\";\n\n",
					footer: "})(window);",
					process: function(src) {
						return src
							.replace(/\s*['"]use strict['"];\n/g, "")
							.replace(/VERSION = ['"].*['"];/, 'VERSION = "' + pkg.version + '";');
					}
				},
				files: {
					"dist/uTarget.js": [
						"src/uTarget.js"
					]
				}
			}
		},
		copy: {
			dev: {
				files: [
					{ expand: false, src: "dist/uTarget.js", dest: "dist/uTarget.min.js" }
				]
			}
		},
		uglify: {
			dist: {
				options: {
					banner: banner
				},
				files: {
					"dist/uTarget.min.js": [
						"dist/uTarget.js"
					]
				}
			}
		},
		karma: {
			options: {
				configFile: "karma.conf.js",
				singleRun: true
			},
			unit: {
				logLevel: "ERROR",
				reporters: "dots"
			},
			full: {
				reporters: "progress"
			}
		},
		jshint: {
			lint: {
				options: {
					browser: true,
					camelcase: true,
					curly: true,
					eqeqeq: true,
					es3: true,
					forin: true,
					freeze: true,
					immed: true,
					indent: 4,
					latedef: true,
					maxlen: 120,
					newcap: true,
					noarg: true,
					noempty: true,
					nomen: true,
					nonbsp: false,
					nonew: true,
					plusplus: false,
					quotmark: true,
					undef: true,
					strict: false,
					trailing: true,
					unused: true,
					globals: {
						Mingos: true
					}
				},
				files: {
					src: ["src/**/*.js"]
				}
			}
		},
		watch: {
			src: {
				files: ["src/**/*.js"],
				tasks: ["jshint:lint", "karma:unit", "concat:dist", "copy:dev"]
			},
			test: {
				files: ["test/**/*.js"],
				tasks: ["karma:unit"]
			}
		}
	});

	// Load tasks
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-copy");
	grunt.loadNpmTasks("grunt-contrib-jshint");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-karma");

	// Custom tasks
	grunt.registerTask("default", [
		"concat:dist",
		"copy:dev"
	]);
	grunt.registerTask("dist", [
		"concat:dist",
		"uglify:dist"
	]);
	grunt.registerTask("test", [
		"jshint:lint",
		"karma:full"
	]);
};
