window.Mingos = window.Mingos || {};

/**
 * Convert the input to an array of Nodes
 * @param   {Node|NodeList|Array|String} target
 * @returns {Array}
 */
var convertTarget = function(target)
{
	"use strict";

	// for arrays, do nothing
	if (target instanceof Array) {
		return target;
	}

	// for Nodes, return an array with one element
	if (target instanceof Node) {
		return [target];
	}

	// for strings, use them to launch the querySelectorAll
	if ("string" === typeof target) {
		target = document.querySelectorAll(target);
	}

	// NodeLists become simple arrays
	if (target instanceof NodeList) {
		var i = 0, j = target.length, output = [];
		for (; i < j; ++i) {
			output[i] = target[i];
		}

		return output;
	}

	throw new TypeError("The argument is expected to be a Node, a NodeList, an array or a string.");
};

/**
 * Run the target appender; find all hyperlinks and add a "target" property to them according to the passed in rules.
 * Default rules will set target="_blank" on all external links and target="_self" on internal ones.
 * @param   {Object} config Optional configuration object with the followinf possible keys:
 *                          "target": {HTMLElement|Array|NodeList|String} target element or elements. If passed in as
 *                                    a string, it will become an argument for document.querySelectorAll.
 *                          "rules": {Object} object containing hosts as keys and the desired targets as values, e.g.:
 *                                   {
 *                                       "wikipedia.org": "_self"
 *                                   }
 * @returns {Number}        Number of altered hyperlinks
 */
Mingos.uTarget = function(config)
{
	"use strict";

	// since config is optional, make sure it exists
	config = config || {};

	// target needs to be an array of nodes or a NodeList object (defaults to document)
	config.target = config.target || [document];
	config.target = convertTarget(config.target);

	// rules are empty by default
	config.rules = config.rules || {};

	// count of altered links
	var altered = 0;

	// create rule regexes:
	var ruleRegexes = [];
	for (var rule in config.rules) {
		// only strings that contain characters other than alnum + dots are considered regexes
		if (config.rules.hasOwnProperty(rule) && rule.match(/[^a-z0-9\.]/)) {
			ruleRegexes.push({ regex: new RegExp(rule), target: config.rules[rule] });
			// since it's a regex, don't check it as a regular rule
			delete config.rules[rule];
		}
	}

	// iterate over the target nodes...
	config.target.forEach(function(item) {
		// find all links
		var links = item.querySelectorAll("a"),
			i = 0,
			j = links.length,
			k,
			l = ruleRegexes.length;

		// iterate over all the links...
		for (; i < j; ++i) {
			var a = links[i];
			// if the target is already set, leave it.
			if (!a.target) {
				if (config.rules[a.host]) {
					// if the target host has a specific rule for it, use that
					a.target = config.rules[a.host];
				} else {
					// match regex rules
					for (k = 0; k < l; ++k) {
						if (ruleRegexes[k].regex.exec(a.host)) {
							a.target = ruleRegexes[k].target;
							break;
						}
					}
				}

				// if still there is no target, perform make default choice:
				// _blank for all external links and _self for internal
				if (!a.target) {
					a.target = (a.host === location.host ? "_self" : "_blank");
				}

				altered++;
			}
		}
	});

	return altered;
};

Mingos.uTarget.VERSION = "1.0.0";
